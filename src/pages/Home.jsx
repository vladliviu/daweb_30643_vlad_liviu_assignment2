import React, { Component } from "react";
import "./Home.css";
import { withNamespaces } from "react-i18next";

function HomePage({ t }) {
  return (
    <div>
      <section>
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-6 order-lg-2">
              <div class="p-5">
                <img
                  class="img-fluid rounded-circle"
                  src="https://gomyprocurement.com/wp-content/uploads/2019/10/Header-manufacturing.jpg"
                  alt=""
                />
              </div>
            </div>
            <div class="col-lg-6 order-lg-1">
              <div class="p-5">
                <h2 class="display-4">{t("home.Industry")}</h2>
                <p>{t("home.IndustryD")}</p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section>
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-6">
              <div class="p-5">
                <img
                  class="img-fluid rounded-circle"
                  src="https://miro.medium.com/max/2400/1*c_fiB-YgbnMl6nntYGBMHQ.jpeg"
                  alt=""
                />
              </div>
            </div>
            <div class="col-lg-6">
              <div class="p-5">
                <h2 class="display-4">{t("home.MachineLearning")}</h2>
                <p>{t("home.MachineLearningD")}</p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section>
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-6 order-lg-2">
              <div class="p-5">
                <img
                  class="img-fluid rounded-circle"
                  src="https://lh3.googleusercontent.com/proxy/uqdlSTP5y4YGsY-fX1siire-mtHlWuTGUC2OOCCaY8uvtHQRBcaekryw4OJs3skfdGCAyOOuIaxvY4gowhSSBkGpt96YhJmTun24J9c_1tVKkzCWrw"
                  alt=""
                />
              </div>
            </div>
            <div class="col-lg-6 order-lg-1">
              <div class="p-5">
                <h2 class="display-4">{t("home.LifeCycle")}</h2>
                <p>{t("home.LifeCycleD")}</p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
export default withNamespaces()(HomePage);
