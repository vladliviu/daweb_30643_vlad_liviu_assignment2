import React, { Component } from "react";
import { withNamespaces } from "react-i18next";

function About({ t }) {
  return (
    <div class="container">
      <div class="row align-items-center my-5">
        <div class="col-lg-7">
          <img
            class="img-fluid rounded mb-4 mb-lg-0"
            src="https://www.encompass-inc.com/wp-content/uploads/2015/02/Encompass_CustomerPudding-900x400.jpg"
            alt=""
          />
        </div>

        <div class="col-lg-5">
          <h1 class="font-weight-light"></h1>
          <p>{t("about.Description")}</p>
          <a
            class="btn btn-primary"
            target="_blank"
            href="https://en.wikipedia.org/wiki/Product_lifecycle"
          >
            {t("about.MorePLM")}
          </a>
        </div>
      </div>
      <div class="card text-white bg-dark my-5 py-4 text-center">
        <div class="card-body">
          <p class="text-white m-0">{t("about.ERP")}</p>
        </div>
      </div>

      <div class="row">
        <div class="col-md-4 mb-5">
          <div class="card h-100">
            <div class="card-body">
              <h2 class="card-title">Machine Learning</h2>
              <p class="card-text">{t("about.ML")}</p>
            </div>
            <div class="card-footer">
              <a
                href="https://en.wikipedia.org/wiki/Machine_learning"
                class="btn btn-primary btn-sm"
                target="_blank"
              >
                {t("about.More")}
              </a>
            </div>
          </div>
        </div>
        <div class="col-md-4 mb-5">
          <div class="card h-100">
            <div class="card-body">
              <h2 class="card-title">Sistem Comunicare</h2>
              <p class="card-text">{t("about.Communication")}</p>
            </div>
            <div class="card-footer">
              <a
                href="https://users.cs.northwestern.edu/~fabianb/classes/msit-p2p-w08/lectures/01-4-IPC.pdf"
                class="btn btn-primary btn-sm"
                target="_blank"
              >
                {t("about.More")}
              </a>
            </div>
          </div>
        </div>
        <div class="col-md-4 mb-5">
          <div class="card h-100">
            <div class="card-body">
              <h2 class="card-title">Aplicatia</h2>
              <p class="card-text">{t("about.Application")}</p>
            </div>
            <div class="card-footer">
              <a
                href="https://en.wikipedia.org/wiki/Client%E2%80%93server_model"
                class="btn btn-primary btn-sm"
                target="_blank"
              >
                {t("about.More")}
              </a>
            </div>
          </div>
        </div>
      </div>
      <br></br>
      <div>
        {" "}
        Couldn't find embeded videos on this topic that would work so i chose
        those ones
      </div>
      <div class="embed-responsive embed-responsive-16by9">
        <iframe
          class="embed-responsive-item"
          src="https://www.youtube.com/embed/v64KOxKVLVg"
          allowfullscreen
        ></iframe>
      </div>
      <br></br>
      <div class="embed-responsive embed-responsive-16by9">
        <iframe
          class="embed-responsive-item"
          src="https://player.vimeo.com/video/137857207"
          allowfullscreen
        ></iframe>
      </div>
    </div>
  );
}
export default withNamespaces()(About);
